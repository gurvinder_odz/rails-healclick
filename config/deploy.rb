require 'capistrano/ext/multistage'
require 'bundler/capistrano'
require 'capistrano-helpers/bundler'
require 'rvm/capistrano'
require 'delayed/recipes'
require 'whenever/capistrano'
require 'pry'

# RVM
set :using_rvm,             true
set :rvm_ruby_string,       "ruby-2.0.0-p353@healclick"
set :rvm_type,              :system
set :rvm_install_with_sudo, true

set :passenger_version,     '4.0.3'
set :passenger_ruby, 'ruby-2.0.0-p353'

set :application,      'healclick'

set :scm,              :git
set :repository,       'git@bitbucket.org:healclick/healclick.git'
set :scm_passphrase,   ''

# aws_live - delayed job
# aws_large, aws_large_second - two production servers
# cap production deploy - would deploy to both
set :stages,           ["aws_staging", "aws_live", "production", "aws_large", "aws_large_second"]
set :default_stage,    "aws_staging"
set :deploy_via,       :remote_cache

set :bundle_flags, ""

set :delayed_job_command, "bin/delayed_job"
set :delayed_job_server_role, :delayed_job

ssh_options[:forward_agent] = true
default_run_options[:pty] = true
set :use_sudo, false

#FIXME: this should not be user-specific to avoid conflicts
set :private_key,       "~/.ssh/HealClickJoey.pem"
set :git_private_key,   "~/.ssh/healclick_deploy" # private key to access git repository

set :whenever_command, "bundle exec whenever"

set :newrelic_license_key, 'fa110f1e94153b26f9530b762019e650b25d87f9'

set :keep_releases, 2

# Database Dumps/Restores
set :db_filename,   DateTime.now.strftime("Dump-%b-%d.sql")


namespace :setup do

  task :server do
    rvm.install_rvm
    rvm.install_ruby
    upload_git_key
    make_apps_dir
    deploy.setup
    install_bundler
    install_nginx
    upload_config
  end

  task :logrotate do
    sudo_put File.read('config/deploy/logrotate_healclick'), "/etc/logrotate.d/logrotate_healclick"
  end

  task :make_apps_dir do
    run "if [ ! -d /apps ]; then sudo mkdir /apps && sudo chown #{user}:#{user} /apps; fi"
  end

  task :install_packages do
    hostname = find_servers_for_task(current_task).first
    exec "ssh -l #{user} #{hostname} -i #{private_key} -t 'sudo apt-get update && sudo apt-get install nginx git-core mysql-server mysql-client libmysqlclient-dev libcurl4-openssl-dev build-essential imagemagick libmagickwand-dev openjdk-6-jdk libreadline6-dev libyaml-dev libsqlite3-dev sqlite3 autoconf libgdbm-dev libncurses5-dev automake bison libffi-dev'"
  end

  task :upload_git_key do
    hostname = find_servers_for_task(current_task).first
    run_locally "scp #{git_private_key} #{user}@#{hostname}:#{git_private_key}"
    run "ssh-agent -s && ssh-add #{git_private_key}"
  end

  task :upload_config do
    run "if [ ! -d #{shared_path}/config ]; then mkdir #{shared_path}/config; fi"
    {}.merge(config_files).each do |remote_filename, local_filename|
      top.upload( local_filename, "#{shared_path}/config/#{remote_filename}", :via => :scp)
    end
  end

  task :install_bundler do
    run "sudo rvm_path=/usr/local/rvm /usr/local/rvm/bin/rvm-shell '#{rvm_ruby_string}' -c 'gem install bundler'"
  end

  task :install_nginx do
    run "rvm_path=/usr/local/rvm /usr/local/rvm/bin/rvm-shell '#{rvm_ruby_string}' -c 'gem install passenger -v #{passenger_version}'"
    run "sudo rvm_path=/usr/local/rvm /usr/local/rvm/bin/rvm-shell '#{rvm_ruby_string}' -c 'passenger-install-nginx-module --auto --auto-download --prefix=/opt/nginx'"
    sudo_put File.read('config/deploy/nginx'), "/etc/init.d/nginx"
    run "sudo chmod +x /etc/init.d/nginx"
    update_nginx_config
  end

  task :update_nginx_config do
    config = File.read("config/deploy/nginx.conf.#{rails_env}") % {
      :passenger_version => passenger_version,
      :passenger_ruby => passenger_ruby,
      :ruby_string => rvm_ruby_string,
      :root => deploy_to, :rails_env => rails_env,
      :nginx_server_name => nginx_server_name
    }
    sudo_put config, "/opt/nginx/conf/nginx.conf"

  end

  task :s3 do
    hostname = find_servers_for_task(current_task).first
    exec "ssh -l #{user} #{hostname} -i #{private_key} -t 'sudo apt-get install s3cmd && s3cmd --configure'"
  end

  task :server_monitoring do
    run "sudo su - root -c 'echo deb http://apt.newrelic.com/debian/ newrelic non-free >> /etc/apt/sources.list.d/newrelic.list'"
    run "sudo su - root -c 'wget -O- https://download.newrelic.com/548C16BF.gpg | apt-key add -'"
    run "sudo apt-get update"
    run "sudo apt-get install newrelic-sysmond"
    run "sudo nrsysmond-config --set license_key=#{newrelic_license_key}"
    run "sudo /etc/init.d/newrelic-sysmond start"
  end
end

namespace :deploy do

  desc "Start application"
  task :start, :roles => :app, :except => { :no_release => true } do
    #run "cd #{current_path} && bundle exec unicorn_rails -E #{rails_env} -c config/unicorn.rb -D"
    run 'sudo /etc/init.d/nginx start'
  end

  desc "Stop application"
  task :stop, :roles => :app, :except => { :no_release => true }  do
    #run "if [ -f #{unicorn_pid} ] && [ -e /proc/$(cat #{unicorn_pid}) ]; then kill -QUIT `cat #{unicorn_pid}`; fi"
    run 'sudo /etc/init.d/nginx stop'
  end

  desc "Restart application"
  task :restart, :roles => :app, :except => { :no_release => true } do
    stop
    start
  end

  desc "Create additional symlinks"
  task :symlink_configs, :role => :app do
    run "ln -nfs #{shared_path}/config/database.yml #{latest_release}/config/database.yml"
    #run "ln -nfs #{shared_path}/config/unicorn.rb #{latest_release}/config/unicorn.rb"
  end
end

namespace :solr do
  task :start do
    run("cd #{current_path} && RAILS_ENV=#{rails_env} bundle exec rake sunspot:solr:start")
  end

  task :reindex do
    run("cd #{current_path} && RAILS_ENV=#{rails_env} bundle exec rake sunspot:solr:reindex")
  end

  task :stop do
    run("if [ -d \"#{current_path}\" ]; then cd #{current_path} && RAILS_ENV=#{rails_env} bundle exec rake sunspot:solr:stop; fi")
  end
end

desc "View logs in real time"
namespace :logs do
  desc "Application log"
  task :application do
    watch_log("cd #{current_path} && tail -f log/#{rails_env}.log")
  end
end

namespace :rails do
  desc "Remote console"
  task :console, :roles => :app do
    hostname = find_servers_for_task(current_task).first
    exec "ssh -l #{user} #{hostname} -i #{private_key} -t \"source ~/.profile && cd #{current_path} && rvm_path=/usr/local/rvm /usr/local/rvm/bin/rvm-shell '#{rvm_ruby_string}' -c 'bundle exec rails c #{rails_env}'\""
  end

  task :runner, :roles => :app do
    hostname = find_servers_for_task(current_task).first
    exec "ssh -l #{user} #{hostname} -i #{private_key} -t \"source ~/.profile && cd #{current_path} && rvm_path=/usr/local/rvm /usr/local/rvm/bin/rvm-shell '#{rvm_ruby_string}' -c 'bundle exec rails runner '#{command}' RAILS_ENV=#{rails_env}'\""
  end
end

namespace :db do
  task :backup, roles: :db do
    run("cd #{current_path} && RAILS_ENV=#{rails_env} bundle exec rake db:backup")
  end
end

namespace :maintenance do
  task :on do
    put File.read('config/deploy/maintenance.html'), "#{current_path}/public/maintenance.html"
  end

  task :off do
    run("cd #{current_path} && rm public/maintenance.html")
  end
end

namespace :delayed_job do

  task :restart_multiple do
    stop
    start_multiple
  end

  task :start_multiple, roles: -> { fetch(:delayed_job_server_role, :app) } do
    counter = 1
    fetch(:delayed_job_queues, {}).each do |queue, count|
      count.to_i.times do
        run "cd #{current_path};#{rails_env} #{delayed_job_command} --queue=#{queue} -i #{counter} start"
        counter = counter.next
      end
    end
  end
end

namespace :database do
  # To dump the production database on staging
  # cap aws_live database:restore_on_staging
  task :restore_on_staging do
    dump
    copy_to_local
    copy_to_staging
    # drop_staging_database
    # import_on_staging
    # run_migrations_on_staging
  end 

  task :restore_on_local do
    dump
    copy_to_local
    import_on_local
  end

  # cap aws_staging database:copy_to_staging
  # TO BE EDITED TO USE THE STAGING SERVER
  task :copy_to_staging do
    # Move the database from your localhost to the staging
    hostname = find_servers_for_task(current_task).first
    your_folder = "/Users/vladcovaliov"
    run_locally "scp #{your_folder}/#{db_filename}.gz #{user}@#{hostname}:/home/ubuntu/"
  end

  # cap aws_staging database:import_on_staging  
  # TO BE EDITED TO USE THE STAGING SERVER
  task :import_on_staging do
    hostname = find_servers_for_task(current_task).first
    cmd = "gzip -d #{db_filename}.gz && mysql -h #{database_host} -u #{database_user} -p\'#{database_password}\' #{database_name} < #{db_filename}"
    exec "ssh -l #{user} #{hostname} -i #{private_key} -t '#{cmd}'"
  end

  # cap aws_staging/aws_live database:drop_staging_database  
  task :run_migrations_on_staging do 
    hostname = find_servers_for_task(current_task).first
    cmd = "cd #{current_path} && RAILS_ENV=staging bundle exec rake db:migrate"
    exec "ssh -l #{user} #{hostname} -i #{private_key} -t \"#{cmd}\""
  end

  # cap aws_staging/aws_live database:drop_staging_database  
  task :drop_staging_database do  
    hostname = find_servers_for_task(current_task).first
    cmd = "cd #{current_path} && RAILS_ENV=staging bundle exec rake db:drop && RAILS_ENV=staging bundle exec rake db:create"
    exec "ssh -l #{user} #{hostname} -i #{private_key} -t '#{cmd}'"
  end

  # cap aws_staging/aws_live database:dump
  task :dump do
    hostname = find_servers_for_task(current_task).first
    # Possible Errors
    # mysqldump: Got errno 28 on write - i.e No space left on the server, df -h to see the available space    
    cmd = "mysqldump -h #{database_host} -u #{database_user} -p\'#{database_password}\' #{database_name} > #{db_filename} && gzip #{db_filename}"

    exec "ssh -l #{user} #{hostname} -i #{private_key} -t '#{cmd}'"
  end

  # cap aws_staging/aws_live database:copy_to_local
  task :copy_to_local do
    hostname = find_servers_for_task(current_task).first
    # Make sure you have the right permissions for this folder
    # chown username folder
    your_folder = "/Users/vladcovaliov"

    run_locally "scp #{user}@#{hostname}:/home/ubuntu/#{db_filename}.gz #{your_folder}/"
  end

  # cap aws_staging/aws_live database:import_on_local
  task :import_on_local do
    user = 'root'
    db_name = 'healclick_development'
    your_folder = '/Users/vladcovaliov'

    run_locally "gzip -d #{your_folder}/#{db_filename}.gz"
    run_locally "mysql -u #{user} -h localhost #{db_name} < #{your_folder}/#{db_filename}"
  end
end

before "deploy:assets:precompile", "deploy:symlink_configs"
after "deploy:update_code", "deploy:migrate"

after "deploy:restart", "deploy:cleanup"
after "deploy:restart", "delayed_job:restart"

after 'deploy:cleanup', 'whenever:update_crontab'
after 'deploy:rollback', 'whenever:update_crontab'

# View logs helper
def watch_log(command)
  raise "Command is nil" unless command
  run command do |channel, stream, data|
    print data
    trap("INT") { puts 'Interupted'; exit 0; }
    break if stream == :err
  end
end

def sudo_put(data, target)
  tmp = "#{shared_path}/~tmp-#{rand(9999999)}"
  put data, tmp
  on_rollback { run "rm #{tmp}" }
  sudo "cp -f #{tmp} #{target} && rm #{tmp}"
end
