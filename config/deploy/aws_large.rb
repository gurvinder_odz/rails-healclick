server 'ec2-54-68-26-177.us-west-2.compute.amazonaws.com', :app, :web, :db, :primary => true
set :deploy_to,         "/apps/healclick"

set :nginx_server_name, 'ec2-54-68-26-177.us-west-2.compute.amazonaws.com'

set :user,             'ubuntu'

set :rails_env,         "production"
set :branch,            "master"

set :config_files, {'database.yml' => 'config/deploy/database.yml'}

role :delayed_job, 'ec2-54-68-26-177.us-west-2.compute.amazonaws.com'