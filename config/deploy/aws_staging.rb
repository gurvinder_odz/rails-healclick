server 'ec2-54-68-4-94.us-west-2.compute.amazonaws.com', :app, :web, :db, :primary => true

set :deploy_to,         "/apps/healclick"

set :nginx_server_name, 'ec2-54-68-4-94.us-west-2.compute.amazonaws.com'

set :user,             'ubuntu'

set :rails_env,         "staging"
set :branch,            "develop"

set :database_host,     "healclickstaging.c8lpl22ahgxq.us-west-2.rds.amazonaws.com"
set :database_user,     "badger"
set :database_password, "badger"
set :database_name,     "healclickstaging"

set :delayed_job_queues, {patient_matches: 1, ongoing_jobs: 1}

role :delayed_job, 'ec2-54-68-4-94.us-west-2.compute.amazonaws.com'
role :assets, 'ec2-54-68-4-94.us-west-2.compute.amazonaws.com'